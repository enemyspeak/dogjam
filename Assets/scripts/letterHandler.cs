﻿using UnityEngine;
using System.Collections;

public class letterHandler : MonoBehaviour {

	public string n;
	public float scale;
	public float targetScale;

	public float delay;

	private TextMesh trenderer;
	private Vector2 startingPosition;
	// Use this for initialization
	void Start () {
		transform.localScale = new Vector2(scale,scale);
		trenderer = GetComponent<TextMesh>();
		trenderer.text = n;
	}
	
	// Update is called once per frame
	void Update () {
		Vector2 currentScale = transform.localScale;
		float newScale = Mathf.Lerp(currentScale.x,targetScale,2.0f * Time.deltaTime);
		transform.localScale = new Vector3(newScale,newScale,1);

		startingPosition = Camera.main.transform.position;
		transform.position = new Vector2(transform.position.x, startingPosition.y - transform.localScale.y/2);
	}
}
