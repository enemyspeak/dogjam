﻿using UnityEngine;
using System.Collections;

public class humanHandler : MonoBehaviour {

	public float leftHorizontal;
	public float rightHorizontal;

	public float minIdleTime;
	public float maxIdleTime;

	public float speed;

	private float target;
	private float idleTime;
	private float idleCounter;

	private bool movingSomewhere;

	private bool NeedsNewPosition;
	private bool direction;

	private Animator anim;

	// Use this for initialization
	void Start () {
		NeedsNewPosition = true;
		anim = GetComponent<Animator>();
	}
	
	// Update is called once per frame
	void Update () {
		if (NeedsNewPosition) {
			think();
			Debug.Log("Human thought about something deep");
		}
		
		movingSomewhere = true;
		
		Vector2 currentPosition = transform.position;

		if (direction) { 		// currentPosition.x <= target
			currentPosition.x += speed * Time.deltaTime;
			Debug.Log(currentPosition.x);
			if (currentPosition.x > target) {
				//movingSomewhere = false;
			}
		} else {		 		// currentPosition.x > target
			currentPosition.x -= speed * Time.deltaTime;
			if (currentPosition.x < target) {
				//movingSomewhere = false;
			}
		}

		if (movingSomewhere) {
			transform.position = currentPosition;
			anim.Play("human-walk");
		} else { 
			anim.Play("human-idle");
		//	transform.position = new Vector2(target,transform.position.y);
			idleCounter += Time.deltaTime;		
			if (idleCounter > idleTime) {
				NeedsNewPosition = true;
			}
		}
	}

	void think() { // HMM
	//	int n = Random.Range(0,4); // this could correspond to 'activities'
		target = Random.Range(leftHorizontal, rightHorizontal);
		direction = false;
		if (target <= transform.position.x) {
			direction = true;
		}
		idleTime = Random.Range(minIdleTime,maxIdleTime);
		idleCounter = 0;
		NeedsNewPosition = false;
	}
}