﻿using UnityEngine;
using System.Collections;

public class lampHandler : MonoBehaviour {

	public Sprite restingState;
	public bool knockedOver = false;

	// Use this for initialization
	void Start () {
		if (knockedOver) {
			SpriteRenderer renderer = GetComponent<SpriteRenderer>();
			renderer.sprite = restingState;
		}
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnTriggerEnter2D (Collider2D c) {
		if (!knockedOver) {
			knockedOver = true;

			Animator anim = GetComponent<Animator>();
			anim.Play("lampfall");
			SpriteRenderer renderer = GetComponent<SpriteRenderer>();
			renderer.sprite = restingState;

			Camera.main.GetComponent<cameraHandler>().verticalOffset = 3f;
		}
	}
}
