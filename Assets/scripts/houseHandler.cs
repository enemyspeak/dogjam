﻿using UnityEngine;
using System.Collections;

public class houseHandler : MonoBehaviour {

	public Object[] roomTiles;
	public GameObject[] rooms;
	public GameObject human;
	public GameObject player;

	public float floorHeight;

	// Use this for initialization
	void Start () {
		int roomLimit = 3;
		float newCenter = 0;
		rooms = new GameObject[roomLimit];

		Vector2 currentPosition = new Vector2(0,0);
		for (int i = 0; i < roomLimit; i++) {
			int maxTiles = roomTiles.Length;
			int n = Random.Range(0,maxTiles);
			GameObject p = Instantiate(roomTiles[n], currentPosition, transform.rotation) as GameObject;
			float width = 12.8f;
			//p.transform.localScale = new Vector2(width,8);
			p.transform.position = new Vector3(newCenter,floorHeight,1);
			if (i+1 != roomLimit) {
				newCenter += width;
			}
			rooms[i] = p;
        }

        // max width for humans
        newCenter += 2.8f;
        human.GetComponent<humanHandler>().rightHorizontal = newCenter;
        Camera.main.GetComponent<cameraHandler>().rightHorizontal = newCenter;
        player.GetComponent<movementHandler>().rightHorizontal = newCenter + 2.6f;
	}
	
	public GameObject getRoomById (int id) {
		return rooms[id];
	}

	// Update is called once per frame
	void Update () {
	
	}
}
