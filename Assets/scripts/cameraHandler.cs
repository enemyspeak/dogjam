﻿using UnityEngine;
using System.Collections;

public class cameraHandler : MonoBehaviour {
	public GameObject player;
//	public GameObject rooms;
	public float rightHorizontal;

	public float verticalOffset;
	private float initialOffset;

	private Vector2 playerPosition;
	private Vector2 target;

	// Use this for initialization
	void Start () {
		playerPosition = player.transform.position;
		target = transform.position;
		transform.position = new Vector2(target.x,target.y + verticalOffset);
		target.y -= verticalOffset;

		initialOffset = verticalOffset;
	}
	
	// Update is called once per frame
	void Update () {
		playerPosition = player.transform.position;
		Vector2 currentPosition = target;
		target = new Vector2(
							Mathf.Lerp( currentPosition.x, playerPosition.x, 3.0f * Time.deltaTime),
							Mathf.Lerp( currentPosition.y, playerPosition.y, 3.0f * Time.deltaTime));
		
		currentPosition = transform.position;
		currentPosition.y -= verticalOffset;

		target = clipCamera( -2.8f,-0.8f,rightHorizontal,1.8f - verticalOffset );

		float currentOffset = verticalOffset;
		verticalOffset = Mathf.Lerp( currentOffset, initialOffset, 1.0f * Time.deltaTime );

		transform.position = new Vector3(
									Mathf.Lerp( currentPosition.x, target.x, 3.0f * Time.deltaTime),
									Mathf.Lerp( currentPosition.y, target.y, 3.0f * Time.deltaTime) + verticalOffset,
									-10);
	}

	Vector2 clipCamera ( float lowerX,float lowerY, float upperX, float upperY ) {
		if (target.x < lowerX) {
			target.x = lowerX;
		} else if (target.x > upperX) {
			target.x = upperX;
		}
		if (target.y < lowerY) {
			target.y = lowerY;
		} else if (target.y > upperY) {
			target.y = upperY;
		}

		return target;
	}
}
