﻿using UnityEngine;
using System.Collections;

public class SpeechBubbleHandler : MonoBehaviour {
	public string text;
	public GameObject Letter;
	public Vector2 startPosition;
	public float letterWidth;

	private GameObject[] letters;

	// Use this for initialization
	void Start () {
		int letterLimit = text.Length;
		letters = new GameObject[letterLimit];

		float textWidth =  letterLimit * letterWidth;
		startPosition.x -= textWidth/2;
		Vector2 currentPosition = startPosition;

		float delayWidth = 0.1f;

		for (int i = 0; i < letterLimit; i++) {
			GameObject l = Instantiate(Letter, currentPosition, transform.rotation) as GameObject;
			l.GetComponent<letterHandler>().n = text.Substring(i, 1);
			l.GetComponent<letterHandler>().delay = delayWidth * i;
			l.transform.position = new Vector2(currentPosition.x + letterWidth,0);
			l.transform.parent = transform;
			l.GetComponent<MeshRenderer>().sortingOrder = 5;
			letters[i] = l;
			currentPosition = l.transform.position;
		}

		transform.position = new Vector2(0, currentPosition.y);
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
